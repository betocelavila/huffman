import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayfairComponent } from './Components/Playfair/playfair.component';
import { VigenereComponent } from './Components/Vigenere/vigenere.component';

export const routes: Routes = [
  {
    path: 'Playfair',
    component: PlayfairComponent,
  },
  {
    path: 'Vigenere',
    component: VigenereComponent,
  },

];


export const CrypthographyRoutingModule = RouterModule.forChild(routes);
