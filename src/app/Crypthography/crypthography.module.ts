import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../Shared/shared.module';
import { CrypthographyRoutingModule } from './crypthography-routing.module';
import { PlayfairComponent } from './Components/Playfair/playfair.component';
import { VigenereComponent } from './Components/Vigenere/vigenere.component';

@NgModule({
  imports: [
    CrypthographyRoutingModule,
    CommonModule,
    SharedModule
  ],
  declarations: [
    PlayfairComponent,
    VigenereComponent
  ],
  exports: [
    PlayfairComponent,
    VigenereComponent
  ],
  providers: [
  ]
})
export class CrypthographyModule { }
