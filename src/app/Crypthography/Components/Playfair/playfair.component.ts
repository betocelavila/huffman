import {
  Component,
  OnInit
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CustomValidators } from '../../../Huffman/Classes/customValidators';


@Component({
  selector: 'app-playfair',
  templateUrl: './playfair.component.html',
  styleUrls: ['./playfair.component.css']
})
export class PlayfairComponent implements OnInit {
  list = new Array<any>();
  matrix = new Array<Array<any>>();
  form: FormGroup;
  formEncrypt: FormGroup;
  formDecrypt: FormGroup;
  resultEncrypt: string;
  resultDecrypt: string;


  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.getForm();
    this.formEncrypt = this.getFormEncrypt();
    this.formDecrypt = this.getFormDecrypy();
  }

  getForm(): FormGroup {
    return this.formBuilder.group({
      input: [{ value: undefined, disabled: false }, [
        CustomValidators.ToUpperCase, CustomValidators.CustomAlphabetOnly, CustomValidators.IsNullorEmpty
      ]]
    });
  }

  getFormDecrypy(): FormGroup {
    return this.formBuilder.group({
      input: [{ value: undefined, disabled: false }, [
        CustomValidators.ToUpperCase, CustomValidators.AlphabetOnly, CustomValidators.IsNullorEmpty
      ]]
    });
  }

  getFormEncrypt(): FormGroup {
    return this.formBuilder.group({
      input: [{ value: undefined, disabled: false }, [
        CustomValidators.ToUpperCase, CustomValidators.AlphabetOnly, CustomValidators.IsNullorEmpty
      ]]
    });
  }

  createMatrix() {
    let aux = 0;
    this.matrix = new Array<Array<any>>();
    this.list = this.keyToArray();
    this.alphabetToArray();
    for (let i = 0; i < 5; i++) {
      this.matrix[i] = new Array<any>();
      for (let j = 0; j < 5; j++) {
        this.matrix[i][j] = this.list[aux];
        aux++;
      }
    }
  }

  keyToArray() {
    const list = new Array<any>();
    for (const iterator of this.form.get('input').value) {
      if (!list.includes(iterator)) {
        if (iterator === 'J') {
          list.push('I');
        } else {
          list.push(iterator);
        }
      }
    }
    return list;
  }

  alphabetToArray() {
    for (let index = 65; index < 91; index++) {
      const char = String.fromCharCode(index);
      if (!this.list.includes(char)) {
        if (char === 'J' && !this.list.includes('I')) {
          this.list.push('I');
        } else if (char === 'I' && !this.list.includes('I')) {
          this.list.push('I');
        } else {
          this.list.push(char);
        }
      }
    }
  }

  encrypt() {
    const diagram = this.makeDigraph(this.formEncrypt.get('input').value);
    const result = [];
    for (const iterator of diagram) {
      result.push(this.encryptPair(iterator));
    }
    this.resultEncrypt = result.join('');
  }

  encryptPair(str) {
    if (str.length !== 2) { return false; }
    const pos1 = this.getCharPosition(str.charAt(0));
    const pos2 = this.getCharPosition(str.charAt(1));
    let char1 = '';
    if (pos1.col === pos2.col) {
      pos1.row++;
      pos2.row++;
      if (pos1.row > 4) { pos1.row = 0; }
      if (pos2.row > 4) { pos2.row = 0; }
      char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    } else if (pos1.row === pos2.row) {
      pos1.col++;
      pos2.col++;
      if (pos1.col > 4) { pos1.col = 0; }
      if (pos2.col > 4) { pos2.col = 0; }
      char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    } else {
      const col1 = pos1.col;
      const col2 = pos2.col;
      pos1.col = col2;
      pos2.col = col1;
      char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    }
    return char1;
  }

  decrypt() {
    const diagram = this.makeDigraph(this.formDecrypt.get('input').value);
    const plaintext = [];
    for (const iterator of diagram) {
      plaintext.push(this.decryptPair(iterator));
    }
    this.resultDecrypt =  plaintext.join('');
  }

  decryptPair(str) {
    if (str.length !== 2) { return false; }
    const pos1 = this.getCharPosition(str.charAt(0));
    const pos2 = this.getCharPosition(str.charAt(1));
    let char1 = '';

    if (pos1.col === pos2.col) {
        pos1.row--;
        pos2.row--;
        if (pos1.row < 0) { pos1.row = 4; }
        if (pos2.row < 0) { pos2.row = 4; }
        char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    } else if (pos1.row === pos2.row) {
        pos1.col--;
        pos2.col--;
        if (pos1.col < 0) { pos1.col = 4; }
        if (pos2.col < 0) { pos2.col = 4; }
        char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    } else {
        const col1 = pos1.col;
        const col2 = pos2.col;
        pos1.col = col2;
        pos2.col = col1;
        char1 = this.getCharFromPosition(pos1) + this.getCharFromPosition(pos2);
    }
    return char1;
}

  getCharPosition(c) {
    const index = this.list.indexOf(c);
    const row = Math.floor(index / 5);
    const col = index % 5;
    return { row, col };
  }

  getCharFromPosition(pos) {
    let index = pos.row * 5;
    index = index + pos.col;
    return this.list.join('').charAt(index);
  }

  makeDigraph(str) {
    const digraph = [];
    str = str.replace('J', 'I');
    str = str.replace(' ', 'X');
    const strArr = str.split('');

    for (let i = 0; i < str.length; i++) {
      if (this.list.indexOf(strArr[i]) === -1) { continue; }
      if (i + 1 >= str.length) {
        digraph.push(strArr[i] + 'X');
      } else if (strArr[i] === strArr[i + 1]) {
        digraph.push(strArr[i] + 'X');
      } else { digraph.push(strArr[i] + strArr[++i]); }
    }
    return digraph;
  }
}
