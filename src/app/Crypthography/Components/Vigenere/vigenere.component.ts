import { OnInit, Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../../../Huffman/Classes/customValidators';

@Component({
  selector: 'app-vigenere',
  templateUrl: './vigenere.component.html',
  styleUrls: ['./vigenere.component.css']
})
export class VigenereComponent implements OnInit {
  form: FormGroup;
  result: string;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.getForm();
  }

  getForm(): FormGroup {
    return this.formBuilder.group({
      input: [{ value: undefined, disabled: false }, [
        CustomValidators.ToUpperCase, CustomValidators.AlphabetOnly, CustomValidators.IsNullorEmpty
      ]],
      key: [{ value: undefined, disabled: false }, [
        CustomValidators.ToUpperCase, CustomValidators.AlphabetOnly, CustomValidators.IsNullorEmpty
      ]]
    });
  }

  doCrypt(isDecrypt) {
    const key = this.filterKey(this.form.get('key').value);

    if (isDecrypt) {
      for (let i = 0; i < key.length; i++) {
        key[i] = (26 - key[i]) % 26;
      }
    }
    this.result = this.crypt(this.form.get('input').value, key);
  }

  crypt(input, key) {
    let output = '';
    for (let i = 0, j = 0; i < input.length; i++) {
      const c = input.charCodeAt(i);
      output += String.fromCharCode((c - 65 + key[j % key.length]) % 26 + 65);
      j++;
    }
    return output;
  }

  filterKey(key) {
    const result = [];
    for (let i = 0; i < key.length; i++) {
      const c = key.charCodeAt(i);
      result.push((c - 65) % 32);
    }
    return result;
  }

}
