import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => import('./Shared/shared.module').then(module => module.SharedModule)
  // },
  // {
  //   path: '',
  //   loadChildren: () => import('./Crypthography/crypthography.module').then(module => module.CrypthographyModule)
  // },
  // {
  //   path: '',
  //   loadChildren: () => import('./Huffman/huffman.module').then(module => module.HuffmanModule)
  // },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
