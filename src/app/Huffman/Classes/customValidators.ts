import { AbstractControl, ValidatorFn, ValidationErrors, FormGroup, Validators } from '@angular/forms';

export class CustomValidators {

  static IsNullorEmpty(control: AbstractControl) {
    if (
      control.value === null ||
      control.value === undefined ||
      control.value === 'undefined' ||
      control.value === ''
    ) {
      return { required: true };
    } else {
      return null;
    }
  }


  static Numeric(control: AbstractControl) {
    if (control.value === null || control.value === undefined || control.value === '') {
      return null;
    }
    if ((/[^0-9]/.test(control.value))) {
      control.setValue(control.value.toString().replace(/[^0-9]*/g, ''));
      return null;
    }
  }

  static CustomNumeric(control: AbstractControl) {
    if (control.value === null || control.value === undefined || control.value === '') {
      return null;
    }
    if ((/[^0-9,./]/.test(control.value))) {
      control.setValue(control.value.toString().replace(/[^0-9]*/g, ''));
      return null;
    }
  }

  static CustomAlphabetOnly(control: AbstractControl) {
    if (control.value !== undefined) {
      if ((/[^a-zA-Z]/.test(control.value))) {
        control.setValue(control.value.replace(/[^a-zA-Z]*/g, ''));
        // return null;
      }
      if ((/[J]/.test(control.value))) {
        control.setValue(control.value.replace(/[J]*/g, ''));
      }
      return null;
    }
  }

  static AlphabetOnly(control: AbstractControl) {
    if (control.value !== undefined) {
      if ((/[^a-zA-Z]/.test(control.value))) {
        control.setValue(control.value.replace(/[^a-zA-Z]*/g, ''));
        // return null;
      }
      return null;
    }
  }

  static AlphabetWhitSpace(control: AbstractControl) {
    if (control.value !== undefined) {
      if ((/[^a-zA-Z ]/.test(control.value))) {
        control.setValue(control.value.replace(/[^a-zA-Z]*/g, ''));
        // return null;
      }
      return null;
    }
  }

  static ToUpperCase(control: AbstractControl) {
    if (control.value && (/[a-z]/.test(control.value))) {
      control.setValue(control.value.toUpperCase());
    }
    return null;
  }

}
