export interface Nodo {
  value: number;
  left?: Nodo;
  rigth?: Nodo;
  id?: number;
  codeVale?: string;
  text?: any;
  children?: any[];
  level?: number;
}
