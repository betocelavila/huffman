import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../Shared/shared.module';
import { HuffmanRoutingModule } from './weather-routing.module';
import { HuffmanComponent } from './Components/huffman.component';

@NgModule({
  imports: [
    HuffmanRoutingModule,
    CommonModule,
    SharedModule
  ],
  declarations: [
    HuffmanComponent
  ],
  exports: [
    HuffmanComponent
  ],
  providers: [
  ]
})
export class HuffmanModule { }
