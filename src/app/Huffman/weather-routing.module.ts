import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HuffmanComponent } from './Components/huffman.component';

export const routes: Routes = [

    {
        path: 'Huffman',
        component: HuffmanComponent,
    },

];


export const HuffmanRoutingModule = RouterModule.forChild(routes);
