import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Nodo } from '../Classes/Nodo';
import { CustomValidators } from '../Classes/customValidators';

declare var vis: any;
declare var Treant: any;

@Component({
  selector: 'app-huffman',
  templateUrl: './huffman.component.html',
  styleUrls: ['./huffman.component.css']
})
export class HuffmanComponent implements OnInit {
  form: FormGroup;
  @ViewChild('dibujo', { static: false }) dibujo: ElementRef;
  nodesResult = new Array<any>();
  codeResult: Array<any> = new Array<any>();
  aux = 0;
  heigth = 0;
  entropy = 0;
  nResult = 0;
  result = 0;
  options = {
    height: '600px',
    layout: {
      hierarchical: {
        enabled: true,
        nodeSpacing: 220,
        blockShifting: false,
        parentCentralization: false,
        edgeMinimization: true,
        direction: 'DU',
        sortMethod: 'directed',
      },
    },
    nodes: {
      shape: 'text',
      size: 10,
      font: {
        size: 12,
        color: 'rgba(0,0,0,.87)'
      },
      // title: '',
      borderWidth: 2
    },
    edges: {
      smooth: {
        type: 'cubicBezier',
        forceDirection: 'vertical',
        roundness: 0.25
      },
    },
    interaction: {
      dragNodes: false,
      zoomView: true,
      dragView: true,
      hover: true,
      tooltipDelay: 300
    }
  };

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.getForm();
  }

  getForm(): FormGroup {
    return this.formBuilder.group({
      inputArray: [{ value: undefined, disabled: false }, [CustomValidators.CustomNumeric, CustomValidators.IsNullorEmpty]]
    });
  }

  formatNumber(input: string) {
    const operands = input.split('/');
    if (operands.length > 1) {
      return Number(operands[0]) / Number(operands[1]);
    } else {
      return Number(input);
    }
  }

  resolve() {
    this.nodesResult = new Array<number>();
    const input = (this.form.get('inputArray').value as string).split(',');
    const nodes: Array<Nodo> = new Array<Nodo>();
    input.forEach(x => {
      nodes.push({
        value: this.formatNumber(x)
      });
    });

    this.entropy = 0;
    for (const key in input) {
      if (input.hasOwnProperty(key)) {
        this.entropy += this.formatNumber(input[key]) * Math.log2(1 / this.formatNumber(input[key]));
      }
    }

    this.entropy = Math.round(this.entropy * 100) / 100;

    nodes.sort((a, b) => {
      return b.value - a.value;
    });

    // console.log('pre', nodes);
    const copyNodes = [...nodes];
    this.createNodes(copyNodes);
    // console.log('post', copyNodes);
    this.setIdNode(copyNodes[0]);
    // console.log('setId', copyNodes);
    this.heigth = this.maximumHeight(copyNodes[0]);
    this.getNodesPre(copyNodes[0], { value: 0 }, 0);
    // console.log('setId', this.nodesResult);
    this.createGraph(copyNodes);

  }

  createGraph(copyNodes: Nodo[]) {
    const nodesGraph = [];
    let coord = 0;
    this.nodesResult.forEach(x => {
      coord = coord + 5;
      nodesGraph.push({
        id: x.id,
        x: coord, y: coord,
        label: x.codeValue !== undefined ? 'Value: ' + x.value.toString() + ' \nCode: ' + x.codeValue : 'Value: ' + x.value.toString()
      });
    });

    // Arreglo de conexiones
    const edgesGraph = [];
    this.nodesResult.forEach(x => {
      edgesGraph.push({
        from: x.id,
        to: x.idFather
      });
    });

    // console.log(nodesGraph);
    // console.log(edgesGraph);


    // console.log('height', this.heigth);

    // console.log(this.nodesResult.filter(x => x.level === 1));

    this.printCodification(this.nodesResult.filter(x => x.level === 1));

    const container = document.getElementById('dibujo');
    const dataDraw = {
      nodes: nodesGraph,
      edges: edgesGraph
    };
    const network = new vis.Network(container, dataDraw, this.options);
  }

  maximumHeight(raiz: Nodo) {
    if (raiz !== undefined) {
      raiz.level = 1 + Math.max(this.maximumHeight(raiz.left), this.maximumHeight(raiz.rigth));
      return 1 + Math.max(this.maximumHeight(raiz.left), this.maximumHeight(raiz.rigth));
    } else {
      return 0;
    }
  }

  printCodification(nodes: Array<any>) {
    this.codeResult = new Array<any>();
    this.nResult = 0;
    nodes.forEach(x => {
      this.codeResult.push({
        value: x.value,
        codeValue: x.codeValue
      });
      this.nResult += (x.value * x.codeValue.length);
    });
    this.result = this.entropy / this.nResult;
  }

  getNodesPre(input: Nodo, father: Nodo, id, side = '') {
    if (input !== undefined) {
      if (father.value === input.value) {
        input.codeVale = father.codeVale;
      } else {
        if (side === 'rigth') {
          input.codeVale = father.codeVale !== undefined ? father.codeVale + '0' : '0';
        } else if (side === 'left') {
          input.codeVale = father.codeVale !== undefined ? father.codeVale + '1' : '1';
        } else {
          input.codeVale = undefined;
        }
      }

      this.nodesResult.push({
        id: input.id,
        value: input.value,
        fatherValue: father.value,
        idFather: father.id,
        codeValue: input.codeVale,
        level: input.level
      });

      this.getNodesPre(
        input.left,
        {
          id: input.id,
          value: input.value,
          codeVale: input.codeVale,
        },
        id + 1,
        'left'
      );

      this.getNodesPre(
        input.rigth,
        {
          id: input.id,
          value: input.value,
          codeVale: input.codeVale
        },
        id + 1,
        'rigth'
      );
    }
  }

  setIdNode(input: Nodo) {
    if (input !== undefined) {
      this.aux = this.aux + 1;
      input.id = this.aux;
      this.setIdNode(input.left);
      this.setIdNode(input.rigth);
    }
  }

  createNodes(input: Array<Nodo>) {
    if (input.length === 1) {
      return input;
    } else {
      let node: Nodo;
      node = {
        left: input[input.length - 2],
        rigth: input[input.length - 1],
        value: input[input.length - 1].value + input[input.length - 2].value,
        text: { name: (input[input.length - 1].value + input[input.length - 2].value).toString() },
        children: [input[input.length - 2], input[input.length - 1]]
      };

      input[input.length - 2] = node;
      input.splice(input.length - 1, 1);

      for (let index = 0; index <= input.length - 2; index++) {
        const aux = JSON.parse(JSON.stringify(input[index]));
        input[index] = {
          value: input[index].value,
          left: aux
        };
      }

      input.sort((a, b) => {
        return b.value - a.value;
      });
      this.createNodes(input);
    }
  }
}
