import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './Shared/shared.module';
import { CrypthographyModule } from './Crypthography/crypthography.module';
import { HuffmanModule } from './Huffman/huffman.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CrypthographyModule,
    HuffmanModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
