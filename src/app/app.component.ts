import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

export interface Nodo {
  value: number;
  left?: Nodo;
  rigth?: Nodo;
  id?: number;
}

declare var vis: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'huffman';
  constructor() {}
}
