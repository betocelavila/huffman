import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {
  MatGridListModule,
  MatCardModule, MatInputModule,
  MatButtonModule, MatProgressSpinnerModule,
  MatToolbarModule, MatIconModule, MatRippleModule, MatTabsModule } from '@angular/material';
import { MenuComponent } from './Components/Menu/menu.component';
import { SharedRoutingModule } from './shared-routing.module';

@NgModule({
    imports: [
      SharedRoutingModule,
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatProgressSpinnerModule,
      MatInputModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatToolbarModule,
      MatIconModule,
      MatRippleModule,
      MatTabsModule
    ],
    declarations: [
      MenuComponent
    ],
    exports: [
      MenuComponent,
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      MatProgressSpinnerModule,
      MatInputModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatToolbarModule,
      MatIconModule,
      MatRippleModule,
      MatTabsModule
    ],
    providers: [
    ]
})
export class SharedModule { }
